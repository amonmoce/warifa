package com.example.jone.warifa0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

public class Pay extends AppCompatActivity {

    Toolbar toolbar;

    public void init(){


        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pay);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuinflater =getMenuInflater();
        menuinflater.inflate(R.menu.main, menu);

        return true;
    }
}
