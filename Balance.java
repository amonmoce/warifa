package com.example.jone.warifa0;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.widget.Toast;

public class Balance extends AppCompatActivity {

    Toolbar toolbar;
    TextView textView;
    public static final String DEFAULT="Y";


    public void init(){

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

            }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuinflater =getMenuInflater();
        menuinflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_balance);
        textView = (TextView)findViewById(R.id.textView6);
        init();
    }






    @Override
    protected void onResume() {
        super.onResume();
        //textViewInfo.setText(""+balance);
    }


    public void check(View view)
    {
        SharedPreferences preferences = getSharedPreferences("mybalance", Context.MODE_PRIVATE);
        String Bal=preferences.getString("money",DEFAULT);
        if(Bal.equals(DEFAULT))
        {
            Toast.makeText(this,"No data found",Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "Data found", Toast.LENGTH_LONG).show();
            textView.setText(Bal);
        }


    }


}
