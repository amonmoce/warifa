package com.example.jone.warifa0;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class second extends AppCompatActivity {

    Toolbar toolbar;

    public Button butR;
    public Button butPay;
    public Button inquiry;
    //public Button butRecord;


    public void init(){

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        butR=(Button)findViewById(R.id.butR);
        butR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(second.this,Recharge.class);
                startActivity(toy);
            }
        });

        inquiry=(Button)findViewById(R.id.inquiry);
        inquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent inq= new Intent(second.this,Balance.class);
                startActivity(inq);


            }
        });


        butPay=(Button)findViewById(R.id.butPay);
        butPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(second.this,Pay.class);
                startActivity(toy);
            }
        });



      /*

        butRecord=(Button)findViewById(R.id.butRecord);
       butRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(second.this,Record.class);
                startActivity(toy);
            }
        });       */

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuinflater =getMenuInflater();
        menuinflater.inflate(R.menu.main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

            return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
