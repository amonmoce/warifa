package com.example.jone.warifa0;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefRecord;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Parcel;
import android.os.Parcelable;


public class Recharge extends AppCompatActivity {

     //Button valider;
     EditText enter_amount;
    Toolbar toolbar;


    public void init(){

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*valider=(Button)findViewById(R.id.valid_R);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuinflater =getMenuInflater();
        menuinflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_recharge);
        enter_amount = (EditText)findViewById(R.id.enter_amount_view);
    }





    public void validate (View view ){

        String amount= enter_amount.getText().toString(); //Get the amount value( in String type)
        double numb = Double.parseDouble(amount); //convert amount value to double type
        byte[] data= new byte[8];//assign to your data in byte type
        long lng = Double.doubleToLongBits(numb);
        for(int i = 0; i < 8; i++)
            data[i] = (byte)((lng >> ((7 - i) * 8)) & 0xff); //the amount is stored in variable data

        String domain = "com.example.jone.warifa0"; //usually your app's package name
        String type = "externalType";
        //NdefRecord extRecord = NdefRecord.createExternal(domain, type, payload);
        NdefRecord external= NdefRecord.createExternal(domain ,type, data);




        //external.writeToParcel(data);


        SharedPreferences preferences = getSharedPreferences("mybalance", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("money",amount);

        editor.apply();

        Toast.makeText(this,"Data saved",Toast.LENGTH_LONG).show();
    }

    public void writeToParcel(Parcel dest, int Flags){

        String amount= enter_amount.getText().toString(); //Get the amount value( in String type)
        double numb = Double.parseDouble(amount); //convert amount value to double type
        byte[] data= new byte[8];//assign to your data in byte type
        long lng = Double.doubleToLongBits(numb);
        for(int i = 0; i < 8; i++)
            data[i] = (byte)((lng >> ((7 - i) * 8)) & 0xff);
        dest.writeByteArray(data);


    };


}

