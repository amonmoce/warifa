package com.example.jone.warifa0;



import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.io.ByteArrayOutputStream;

public class Welcome extends AppCompatActivity  {

     NfcAdapter nfcAdapter;
    Toolbar toolbar;
    public Button butMenu;
    public Button butSetting;
    public Button b_Tag;


    public void init(){

        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        butMenu=(Button)findViewById(R.id.but_menu0);
        butMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(Welcome.this,second.class);
                startActivity(toy);
            }
        });

        b_Tag=(Button)findViewById(R.id.but_tag);
        b_Tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(Welcome.this,readwrite.class);
                startActivity(toy);
            }
        });


        butSetting=(Button)findViewById(R.id.but_setting0);
        butSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toy= new Intent(Welcome.this,SettingsActivity.class);
                startActivity(toy);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        init();
        getSupportActionBar();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

               NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);



        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NFC not available", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Example of a call to a native method
        // TextView tv = (TextView) findViewById(R.id.sample_text);
        //tv.setText(stringFromJNI());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuinflater =getMenuInflater();
        menuinflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
     public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();

         if(id==R.id.action_settings)
         {
             Intent intent= new Intent(this,SettingsActivity.class);
             startActivity(intent);

             return true;
         }
         return super.onOptionsItemSelected(item);
     }


    /*
    @Override
    protected void onNewIntent(Intent intent) {

        Toast.makeText(this, "Tag read", Toast.LENGTH_SHORT).show();
        super.onNewIntent(intent);

    }

    @Override
    protected void onResume() {

        Intent intent= new Intent(this, Welcome.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);
        IntentFilter[] intentFilter = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);

        super.onResume();
    }


*/
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    //public native String stringFromJNI();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


}
